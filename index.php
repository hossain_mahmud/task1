<?php


$twoPairValues = [
    [1, 5],
    [9, -7],
    [0, 8],
    [6, 3],
    [4, 11],
    [14, 0],
    [8, 1],
    [4, 9],
];

$targetValue = 9;

function getTargetIndex($array, $target)
{
    foreach ($array as $key => $value) {
        $sum = $value[0] + $value[1];
        if ($sum == $target && $key == 6) {
            echo $key . '<br>';
        }
    }
}

getTargetIndex($twoPairValues, $targetValue);
